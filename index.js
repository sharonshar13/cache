const express = require('express')
const bodyParser = require('body-parser');
const cacheService = require('./cache-service');
const cluster = require('./cluster');
const app = express()

app.use(bodyParser.json())

app.use((req, res, next)=>{
    // check to see if current node is master
    if(cluster.self.master){
        next();
    }else{
        res.status(400).send("This is not master")
    }
})

app.post('/messages', (req, res) => {
    if (validateDocument(req.body)){
        cacheService.addDocument(req.body);
        cluster.broadcastNewDocument(req.body);
        res.send(req.body.id.toString());
    } else {
        res.status(400).send("Invalid request body.");
    }
})

app.get('/messages/:id', (req, res) => {
    let document = cacheService.getDocument(req.params.id);
    if (document == undefined){
        res.status(404).send('Resource not found.');
    } else {
        res.send({
            id: req.params.id,
            message: document.message
        })
    }
})

app.delete('/messages', (req, res) => {
    cacheService.clearDocuments();
    cluster.broadcastClearDocuments();
    res.send('Documents cleared.');
})

app.listen(process.argv[2], () => console.log('HTTP server listening on port ' + process.argv[2]))

/* validate document */
function validateDocument (document){
    let requiredNum = 2;
    let requiredCount = 0;

    for (let x in document){
        switch(x){
            case "id":
                if(typeof document[x] != "number"){
                    return false;
                }
                requiredCount++;
                break;
            case "message":
                if(typeof document[x] != "string"){
                    return false;
                }
                requiredCount++;
                break;
            case "ttl":
                if(typeof document[x] != "number"){
                    return false;
                }
                break;
            default:
                return false;
        }
    }
    
    if(requiredCount != requiredNum){
        return false;
    }

    return true;
}