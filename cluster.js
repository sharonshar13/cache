const cacheService = require('./cache-service');

var self = {};
var master = {};
var slaves = [];
var io; //master
var socket; //slave

/* initialize parameters */
self = {
    master: process.argv[3] == "master" ? true : false,
    ip: process.argv[4],
    port: process.argv[5],
    timestamp: new Date().getTime()
}

if(self.master){
    master = self;
}else{
    master = {
        master: true,
        ip: process.argv[6],
        port: process.argv[7],
        timestamp: new Date().getTime()
    }
    slaves.push(self);
}

/* start connection */
if(self.master){
    startMaster();
}else{
    startSlave();
}

function startMaster(){
    io = require('socket.io')(master.port);
    console.log('Server listerning on port ' + master.port);

    io.on('connection', (socket) => {
        let currentSlave;

        /* new slave connection came in */
        socket.on('initialize', (slave) => {
            console.log('A slave connected: ' + slave.ip + ':' + slave.port);
            socket.emit('existing-slave', slaves);  //send existing slaves to the new slave
            slaves.push(slave);     //add the new slave to the slave list
            socket.broadcast.emit('new-slave', slave);  //broadcast new slave to all other slaves
            currentSlave = slave;
        })

        /* handle slave disconnection event */
        socket.on('disconnect', () => {
            console.log('A slave disconnected: ' + currentSlave.ip + ':' + currentSlave.port);
            io.emit('slave-disconnected', currentSlave);    //send disconnected slave to all other slaves
            
            let index = slaves.findIndex((tmpSlave)=>{
                return tmpSlave.ip == currentSlave.ip && tmpSlave.port == currentSlave.port;
            })
            slaves.splice(index, 1);  //remove disconnected slave from the slave list
        })
    })
}

function startSlave(){
    socket = require('socket.io-client')("http://" + master.ip + ":" + master.port);

    socket.on('connect', () => {
        console.log("Slave: connected to server.");
        socket.emit('initialize', self);    //send slave itself to master
    })

    /* concat existing slaves to slave list */
    socket.on('existing-slave', (existingSlaves)=>{
        slaves = slaves.concat(existingSlaves);
    })

    /* add new slave to the slave list */
    socket.on('new-slave', (slave)=>{
        console.log('A slave connected: ' + slave.ip + ':' + slave.port);
        slaves.push(slave);
    })

    /* remove disconnected slave from the slave list */
    socket.on('slave-disconnected', (slave)=>{
        let index = slaves.findIndex((tmpSlave)=>{
            return tmpSlave.ip == slave.ip && tmpSlave.port == slave.port;
        })
        console.log('A slave disconnected: ' + slaves[index].ip + ':' + slaves[index].port);
        slaves.splice(index, 1);
    })

    /* handle the disconnection event between slave and master 
     * elect a new master
     * */
    socket.on('disconnect', ()=>{
        console.log('Disconnected from master, start electing new master');
        
        /* find the earliest slave */
        let minimumTimestamp = new Date(8640000000000000).getTime();
        let index;
        let newMaster;
        for(let i = 0; i < slaves.length; i++){
            if(slaves[i].timestamp < minimumTimestamp){
                newMaster = slaves[i];
                minimumTimestamp = slaves[i].timestamp;
                index = i;
            }
        }

        /* update master */
        newMaster.master = true;
        master = newMaster;
        slaves.splice(index, 1);

        /* restart socket */
        if(self.master == true){
            startMaster();
        }else{
            startSlave();
        }
    })

    socket.on('new-document', (document) => {
        cacheService.addDocument(document);
    })

    socket.on('clear-documents', () => {
        cacheService.clearDocuments();
    })
}

module.exports.self = self;

module.exports.broadcastNewDocument = (document) => {
    io.emit('new-document', document);
}

module.exports.broadcastClearDocuments = () => {
    io.emit('clear-documents');
}