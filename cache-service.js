const DEFAULT_TTL = 30000;
var documents = {};

module.exports.addDocument = (document) => {
    if (document.ttl == undefined){
        document.ttl = DEFAULT_TTL;
    }

    let timeout = setTimeout( () => { 
        delete documents[document.id] 
    }, document.ttl)
    documents[document.id] = {
        message: document.message,
        timeout: timeout,
    }
}

module.exports.getDocument = (id) => {
    return documents[id];
}

module.exports.clearDocuments = () => {
    for(let x in documents){
        clearTimeout(documents[x].timeout);
    }
    documents = {};
}