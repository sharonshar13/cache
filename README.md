## Command for starting node
```
node index.js [http_port] [master | slave] [socket_self_ip] [socket_self_port] [socket_master_ip] [socket_master_port]
```


## Local setup example
```
node index.js 3000 master localhost 3010 

node index.js 4000 slave localhost 4010 localhost 3010 

node index.js 5000 slave localhost 5010 localhost 3010 
```

## AWS setup
```
pm2 start index.js --no-autorestart -- 3000 master 18.219.31.0 3010 

pm2 start index.js --no-autorestart -- 3000 slave 18.220.173.21 3010 18.219.31.0 3010 

pm2 start index.js --no-autorestart -- 3000 slave 18.219.103.77 3010 18.219.31.0 3010 
```